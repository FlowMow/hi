package org.openjfx.Controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import org.openjfx.Hilfsklassen.WindowPresetSwitchScene;
import org.openjfx.Hilfsklassen.WindowPresetSwitchStage;
import org.openjfx.hellojfx.LoginWindow;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    private WindowPresetSwitchStage wp = new WindowPresetSwitchStage();
    private WindowPresetSwitchScene wpss = new WindowPresetSwitchScene();
    private LoginWindow lw = new LoginWindow();

    @FXML
    private TitledPane titledPane;

    @FXML
    private Button buttonEinloggen;

    @FXML
    private Button buttonRegistrieren;

    @FXML
    private TextField textFieldUserName;

    @FXML
    private PasswordField PasswordField;

    @FXML
    private ImageView regsiterImage;

    /*Diese Methode wechselt das Fenster vom LogIn Fenster zum Funktionen Fenster durch den Einloggen Button Klick*/
    public void switchToFunktionenWindow(ActionEvent event){
        wp.createWindowNewStage("/funktionen.fxml", "Funktion wählen!", new FunktionController());
        closePreviousWindowLogin();
    }

    /*Diese Methode wechselt die Scene von der LogIn Scene in die Regristrieren Scene durch RegistrierenButton Klick */
    public void switchToRegistrierenWindow(ActionEvent event){
        wpss.createWindowSwitchScene("/registrieren.fxml", new RegistrierenController(), lw.getWindow());
    }

    /*Diese Methode dient dazu das Login Fenster zu schließen nach erfolgreichem Login*/
    public void closePreviousWindowLogin(){
        Stage stageInfo = (Stage) titledPane.getScene().getWindow();
        stageInfo.close();
    }

    /*Hier werden die anklickbaren Button ihren jeweiligen Methoden zugewiesen*/
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        buttonEinloggen.setOnAction(this::switchToFunktionenWindow);
        buttonRegistrieren.setOnAction(this::switchToRegistrierenWindow);
    }
}
