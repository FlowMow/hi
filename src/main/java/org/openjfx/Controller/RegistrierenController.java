package org.openjfx.Controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import org.openjfx.Hilfsklassen.WindowPresetSwitchScene;
import org.openjfx.hellojfx.LoginWindow;

import java.net.URL;
import java.util.ResourceBundle;

public class RegistrierenController implements Initializable {

    private LoginWindow lw = new LoginWindow();
    private WindowPresetSwitchScene wpss = new WindowPresetSwitchScene();

    @FXML
    private PasswordField PasswordField;

    @FXML
    private TextField textFieldVorname;

    @FXML
    private TextField textfieldName;

    @FXML
    private TextField txtEmail;

    @FXML
    private ImageView registerImage;

    @FXML
    private Button btnNeuAnmelden;

    @FXML
    private Label lblKontoErstellen;

    @FXML
    private Button btnZurueck;

    /*Diese Methode wechselt die von der Registrieren Scene zur LogIn Scene*/
    public void switchToLoginWindow(ActionEvent event){
        wpss.createWindowSwitchScene("/loginWindow.fxml", new LoginController(), lw.getWindow());
    }

    /*Hier werden die anklickbaren Button ihren jeweiligen Methoden zugewiesen*/
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnZurueck.setOnAction(this::switchToLoginWindow);
    }
}
