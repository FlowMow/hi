package org.openjfx.hellojfx;
import org.openjfx.Controller.LoginController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

public class LoginWindow extends Application{

    private static Stage window;

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        final URL fxmlUrl = getClass().getResource("/loginWindow.fxml");
        final FXMLLoader fxmlLoader = new FXMLLoader(fxmlUrl);
        fxmlLoader.setController(new LoginController());
        final Parent root = fxmlLoader.load();
        Scene scene = new Scene(root, 500, 500);
        window.setScene(scene);
        window.setTitle("Login Window");
        window.show();
    }

    public Stage getWindow(){
        return window;
    }

    public static void main(String[] args){
        launch(args);
    }
}
